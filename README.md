# Ascii Path

Talented Code Challenge
    Path following algorithm in ASCII Map
    Find the position of character '@'
    Follow the path, stop when character 'x' is reached


Source: https://gist.github.com/tuomasj/8061c6940d74d3ab55bbea582e6c8f24